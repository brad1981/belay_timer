# Take! 
## An app for the fair distribution of belay time amoung climbing partners.

### Motivation: 
Does it ever feel like your friends are spending more time on the sharp end than you? Tired of playing page to their white knight?
This app helps you track how much you belay each other and displays the balance in an easy-to-read donought chart.

